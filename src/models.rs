use crate::schema::{categories, group_members, tasks, todogroups, users};

#[derive(Queryable, Identifiable, Debug)]
#[table_name = "categories"]
pub struct Category {
    pub id: i32,
    pub name: String,
    pub discription: String,
}

#[derive(Insertable)]
#[table_name = "categories"]
pub struct NewCategory<'a> {
    pub name: &'a str,
    pub discription: &'a str,
}

#[derive(Queryable, Identifiable, Associations, PartialEq, Debug, Serialize)]
#[belongs_to(Category, foreign_key = "category")]
#[table_name = "tasks"]
pub struct Task {
    pub id: i32,
    pub name: String,
    pub discription: String,
    pub discription_cache: String,
    pub created: i32,
    pub started: Option<i32>,
    pub due: Option<i32>,
    pub category: Option<i32>,
    pub parent: Option<i32>,
    pub done: Option<i32>,
    pub todogroup: i32,
}

#[derive(Insertable, Deserialize, Queryable)]
#[table_name = "tasks"]
pub struct NewTask<'a> {
    pub name: &'a str,
    pub discription: &'a str,
    pub discription_cache: String,
    pub created: i32,
    pub category: Option<i32>,
    pub parent: Option<i32>,
    pub todogroup: i32,
}

#[derive(Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "users"]
pub struct User {
    pub id: i32,
    pub name: String,
    pub password: String,
    pub email: String,
    pub admin: bool,
    pub validated: bool,
    pub active: bool,
}

#[derive(Insertable, Deserialize)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub name: &'a str,
    pub password: &'a str,
    pub email: &'a str,
    pub validated: bool,
    pub active: bool,
    pub admin: bool,
}

#[derive(Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "todogroups"]
pub struct Todogroup {
    pub id: i32,
    pub name: String,
    pub discription: String,
    pub public_visable: bool,
}

#[derive(Insertable, Deserialize)]
#[table_name = "todogroups"]
pub struct NewTodogroup<'a> {
    pub name: &'a str,
    pub discription: &'a str,
    pub public_visable: bool,
}

#[derive(Queryable, Identifiable, PartialEq, Debug, Serialize)]
#[table_name = "group_members"]
#[primary_key("user", "todogroup")]
pub struct GroupMember {
    pub user: i32,
    pub todogroup: i32,
    pub admin: bool,
}

#[derive(Insertable, Deserialize)]
#[table_name = "group_members"]
pub struct NewGroupMember {
    pub user: i32,
    pub todogroup: i32,
    pub admin: bool,
}
