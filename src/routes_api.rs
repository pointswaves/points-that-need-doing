use diesel::{self, prelude::*};

use rocket_contrib::json::Json;

use crate::models::{NewTask, Task};
use crate::schema;
use crate::DbConn;

#[post("/create_task", data = "<task>")]
pub fn create_task(conn: DbConn, task: Json<NewTask>) -> Result<String, String> {
    let inserted_rows = diesel::insert_into(schema::tasks::table)
        .values(&task.0)
        .execute(&conn.0)
        .map_err(|err| -> String {
            println!("Error inserting row: {:?}", err);
            "Error inserting row into database".into()
        })?;

    Ok(format!("Inserted {} row(s).", inserted_rows))
}

#[get("/list_tasks")]
pub fn list_tasks(conn: DbConn) -> Result<Json<Vec<Task>>, String> {
    use crate::schema::tasks::dsl::*;

    tasks
        .load(&conn.0)
        .map_err(|err| -> String {
            println!("Error querying page views: {:?}", err);
            "Error querying page views from the database".into()
        })
        .map(Json)
}
