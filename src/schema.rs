table! {
    categories (id) {
        id -> Integer,
        name -> Text,
        discription -> Text,
    }
}

table! {
    group_members (user, todogroup) {
        user -> Integer,
        todogroup -> Integer,
        admin -> Bool,
    }
}

table! {
    tasks (id) {
        id -> Integer,
        name -> Text,
        discription -> Text,
        discription_cache -> Text,
        created -> Integer,
        started -> Nullable<Integer>,
        due -> Nullable<Integer>,
        category -> Nullable<Integer>,
        parent -> Nullable<Integer>,
        done -> Nullable<Integer>,
        todogroup -> Integer,
    }
}

table! {
    todogroups (id) {
        id -> Integer,
        name -> Text,
        discription -> Text,
        public_visable -> Bool,
    }
}

table! {
    users (id) {
        id -> Integer,
        name -> Text,
        password -> Text,
        email -> Text,
        admin -> Bool,
        validated -> Bool,
        active -> Bool,
    }
}

joinable!(group_members -> todogroups (todogroup));
joinable!(group_members -> users (user));
joinable!(tasks -> categories (category));

allow_tables_to_appear_in_same_query!(categories, group_members, tasks, todogroups, users,);
