use diesel::{self, prelude::*};
use rocket::request::{FlashMessage, Form, FormDataError, FormError};
use rocket::response::Redirect;
use rocket_contrib::templates::Template;
use std::time::SystemTime;

use crate::models::{Category, GroupMember, NewCategory, NewTask, Task, Todogroup};
use crate::routes_auth::LogedInUser;
use crate::schema;
use crate::DbConn;

use crate::schema::{categories, tasks, todogroups};

#[derive(Debug, Serialize)]
struct TodoItem {
    name: String,
    des: String,
    list_start: bool,
    have_sub: bool,
    end_strings: Vec<String>,
    id: i32,
    cat: Option<String>,
    parent: Option<i32>,
    complete: bool,
}

#[derive(Debug, Serialize)]
struct TemplateData {
    todo_list: Vec<TodoItem>,
}

#[derive(Debug, Serialize)]
struct TemplateError {
    error_type: String,
    error_detail: String,
}

#[derive(Debug, Serialize)]
struct TemplatFlash {
    flash_title: String,
    flash_detail: String,
}

enum ShowItems {
    All,
    Todo,
}

fn recurce_task_list(
    conn: &DbConn,
    task_list: Vec<(Task, Option<Category>)>,
    show_done: &ShowItems,
) -> Result<Vec<TodoItem>, Template> {
    let mut todo_items = vec![];
    for (index, (this_task, this_cat)) in task_list.iter().enumerate() {
        let join = tasks::table.left_join(categories::table);
        let mut query = join.filter(tasks::parent.eq(this_task.id)).into_boxed();
        if let ShowItems::Todo = show_done {
            query = query.filter(tasks::done.is_null());
        }
        let all_data =
            query
                .load::<(Task, Option<Category>)>(&conn.0)
                .map_err(|err| -> Template {
                    println!("Error getting todo items from the database: {:?}", err);
                    Template::render(
                        "errors",
                        &TemplateError {
                            error_detail: format!(
                                "Error getting todo items from the database: {:?}",
                                err
                            ),
                            error_type: "Database Error:".into(),
                        },
                    )
                })?;

        let sub_tasks = recurce_task_list(conn, all_data, &show_done)?;
        let have_subtask: bool = !sub_tasks.is_empty();
        todo_items.push(TodoItem {
            name: this_task.name.clone(),
            des: this_task.discription_cache.clone(),
            list_start: index == 0,
            have_sub: have_subtask,
            end_strings: vec![],
            id: this_task.id,
            cat: this_cat
                .as_ref()
                .map(|category| category.discription.clone()),
            parent: this_task.parent,
            complete: this_task.done.is_some(),
        });
        todo_items.extend(sub_tasks);
        if have_subtask {
            if let Some(last) = todo_items.last_mut() {
                last.end_strings.push("</div>".into())
            }
        }
    }
    if let Some(last) = todo_items.last_mut() {
        if !task_list.is_empty() {
            last.end_strings.push("</ul>".into())
        }
    };

    Ok(todo_items)
}

no_arg_sql_function!(RANDOM, (), "Represents the sql RANDOM() function");

#[get("/")]
pub fn index(
    flash: Option<FlashMessage>,
    conn: DbConn,
    user: Option<LogedInUser>,
) -> Result<Template, Template> {
    if let Some(flash_mesage) = flash {
        Ok(Template::render(
            "flash",
            &TemplatFlash {
                flash_title: flash_mesage.name().to_string(),
                flash_detail: flash_mesage.msg().to_string(),
            },
        ))
    } else if let Some(actual_user) = user {
        let all_data = todo_query(&conn, actual_user.id())
            .order(RANDOM)
            .limit(1)
            .load::<(Task, Option<Category>)>(&conn.0)
            .map_err(|err| -> Template {
                println!("Error getting todo items from the database: {:?}", err);
                Template::render(
                    "errors",
                    &TemplateError {
                        error_detail: format!(
                            "Error getting todo items from the database: {:?}",
                            err
                        ),
                        error_type: "Database Error:".into(),
                    },
                )
            })?;
        let todo_items = recurce_task_list(&conn, all_data, &ShowItems::Todo)?;
        let template_data = TemplateData {
            todo_list: todo_items,
        };
        Ok(Template::render("front_page", template_data))
    } else {
        let template_data = TemplateData { todo_list: vec![] };
        Ok(Template::render("front_guest", template_data))
    }
}

// There are things in Diesel that should allow this to be "derived"
// That will make it much more redable, and also robust.
// If any of the data base tables get updated then this will need updating
#[allow(clippy::type_complexity)]
fn todo_query<'a>(
    conn: &DbConn,
    user_id: i32,
) -> diesel::query_builder::BoxedSelectStatement<
    'a,
    (
        (
            diesel::sql_types::Integer,
            diesel::sql_types::Text,
            diesel::sql_types::Text,
            diesel::sql_types::Text,
            diesel::sql_types::Integer,
            diesel::sql_types::Nullable<diesel::sql_types::Integer>,
            diesel::sql_types::Nullable<diesel::sql_types::Integer>,
            diesel::sql_types::Nullable<diesel::sql_types::Integer>,
            diesel::sql_types::Nullable<diesel::sql_types::Integer>,
            diesel::sql_types::Nullable<diesel::sql_types::Integer>,
            diesel::sql_types::Integer,
        ),
        diesel::sql_types::Nullable<(
            diesel::sql_types::Integer,
            diesel::sql_types::Text,
            diesel::sql_types::Text,
        )>,
    ),
    diesel::query_source::joins::JoinOn<
        diesel::query_source::joins::Join<
            schema::tasks::table,
            schema::categories::table,
            diesel::query_source::joins::LeftOuter,
        >,
        diesel::expression::operators::Eq<
            diesel::expression::nullable::Nullable<schema::tasks::columns::category>,
            diesel::expression::nullable::Nullable<schema::categories::columns::id>,
        >,
    >,
    diesel::sqlite::Sqlite,
> {
    let join_members = schema::group_members::table.inner_join(schema::todogroups::table);
    let todo_groups: std::vec::Vec<(GroupMember, Todogroup)> = join_members
        .filter(schema::group_members::user.eq(user_id))
        .load(&conn.0)
        .unwrap();
    let join = tasks::table.left_join(categories::table);
    let mut query = join.into_boxed();
    for (_group_member, todo_group) in todo_groups.iter() {
        query = query.or_filter(
            tasks::todogroup
                .eq(todo_group.id)
                .and(tasks::parent.is_null())
                .and(tasks::done.is_null()),
        );
    }
    query
}

#[get("/task")]
pub fn task_all(conn: DbConn, user: LogedInUser) -> Result<Template, Template> {
    let query = todo_query(&conn, user.id());

    let all_data = query
        .load::<(Task, Option<Category>)>(&conn.0)
        .map_err(|err| -> Template {
            println!("Error getting todo items from the database: {:?}", err);
            Template::render(
                "errors",
                &TemplateError {
                    error_detail: format!("Error getting todo items from the database: {:?}", err),
                    error_type: "Database Error:".into(),
                },
            )
        })?;
    let todo_items = recurce_task_list(&conn, all_data, &ShowItems::Todo)?;
    let template_data = TemplateData {
        todo_list: todo_items,
    };
    println!("{:?}", template_data);
    Ok(Template::render("list_todo", &template_data))
}

#[get("/task?<task_id>")]
pub fn task_by_id(conn: DbConn, task_id: i32) -> Result<Template, Template> {
    let join = tasks::table.left_join(categories::table);
    let all_data = join
        .filter(tasks::id.eq(task_id))
        .load::<(Task, Option<Category>)>(&conn.0)
        .map_err(|err| -> Template {
            println!("Error getting todo items from the database: {:?}", err);
            Template::render(
                "errors",
                &TemplateError {
                    error_detail: format!("Error getting todo items from the database: {:?}", err),
                    error_type: "Database Error:".into(),
                },
            )
        })?;
    if all_data.len() != 1 {
        Err(Template::render(
            "errors",
            &TemplateError {
                error_detail: "Error getting single view from database".into(),
                error_type: "Database Error:".into(),
            },
        ))
    } else {
        let todo_items = recurce_task_list(&conn, all_data, &ShowItems::All)?;
        let template_data = TemplateData {
            todo_list: todo_items,
        };
        println!("{:?}", template_data);
        Ok(Template::render("list_todo", &template_data))
    }
}

#[derive(Debug, Serialize)]
struct TemplateCat {
    id: i32,
    name: String,
}

#[derive(Debug, Serialize)]
struct TemplateTodoGroup {
    id: i32,
    name: String,
}

#[derive(Debug, Serialize)]
struct TemplateParent {
    id: i32,
    name: String,
}

#[derive(Debug, Serialize)]
struct TemplateTodo {
    cats: Vec<TemplateCat>,
    groups: Vec<TemplateTodoGroup>,
    parent: Option<TemplateParent>,
}

// eg, http://localhost:8000/add_todo?parent=2
// see https://rocket.rs/v0.4/guide/requests/#query-strings
#[get("/add_todo?<parent>")]
pub fn add_todo_form(conn: DbConn, parent: Option<i32>) -> Result<Template, Template> {
    let list_of_cats: Vec<Category> =
        categories::dsl::categories
            .load(&conn.0)
            .map_err(|err| -> Template {
                println!("Error getting catefories from the database: {:?}", err);
                Template::render(
                    "errors",
                    &TemplateError {
                        error_type: "Database Error".to_string(),
                        error_detail: format!(
                            "Error getting catefories from the database: {:?}",
                            err
                        ),
                    },
                )
            })?;
    let list_of_groups: Vec<Todogroup> =
        todogroups::dsl::todogroups
            .load(&conn.0)
            .map_err(|err| -> Template {
                println!("Error getting todogroups from the database: {:?}", err);
                Template::render(
                    "errors",
                    &TemplateError {
                        error_type: "Database Error".to_string(),
                        error_detail: format!(
                            "Error getting todogroups from the database: {:?}",
                            err
                        ),
                    },
                )
            })?;
    let parent_template: Option<TemplateParent> = if let Some(parent_id) = parent {
        let parent_task: Task = tasks::dsl::tasks
            .filter(tasks::id.eq(parent_id))
            .first(&conn.0)
            .map_err(|err| -> Template {
                println!("Error finding parent: {:?}", err);
                Template::render(
                    "errors",
                    &TemplateError {
                        error_type: "Database Error".to_string(),
                        error_detail: format!(
                            "Error finding parent {:?} in database: {:?}",
                            parent_id, err
                        ),
                    },
                )
            })?;
        Some(TemplateParent {
            id: parent_task.id,
            name: parent_task.name,
        })
    } else {
        None
    };
    println!("parent {:?}", parent);
    let context = TemplateTodo {
        cats: list_of_cats
            .into_iter()
            .map(|cat| TemplateCat {
                id: cat.id,
                name: cat.name,
            })
            .collect(),
        groups: list_of_groups
            .into_iter()
            .map(|todogroup| TemplateTodoGroup {
                id: todogroup.id,
                name: todogroup.name,
            })
            .collect(),
        parent: parent_template,
    };
    Ok(Template::render("todo_form", &context))
}

#[get("/add_category")]
pub fn add_category_form() -> Template {
    let context = TemplateData { todo_list: vec![] };
    Template::render("categories_form", &context)
}

#[derive(Debug, FromForm, Deserialize)]
pub struct TodoForm {
    name: String,
    description: String,
    category: Option<i32>,
    parent: Option<i32>,
    group: i32,
}

impl TodoForm {
    pub fn to_task(&self) -> NewTask {
        let html: String = markdown::to_html(&self.description.as_str());
        NewTask {
            name: &self.name,
            discription: &self.description,
            discription_cache: html,
            created: SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs() as i32,
            category: self.category,
            parent: self.parent,
            todogroup: self.group,
        }
    }
}

#[post("/add_todo", data = "<todo_data>")]
pub fn add_todo(
    conn: DbConn,
    todo_data: Result<Form<TodoForm>, FormError>,
    _logged_in_user: LogedInUser,
) -> Result<Redirect, Template> {
    match todo_data {
        Ok(form) => {
            let new_task = form.to_task();

            use crate::diesel::Connection;
            use diesel::result::Error;
            let _inserted_tasks: std::vec::Vec<Task> = conn
                .0
                .transaction::<_, Error, _>(|| {
                    let inserted_count = diesel::insert_into(schema::tasks::dsl::tasks)
                        .values(&new_task)
                        .execute(&conn.0)?;

                    Ok(schema::tasks::dsl::tasks
                        .order(schema::tasks::dsl::id.desc())
                        .limit(inserted_count as i64)
                        .load(&conn.0)?
                        .into_iter()
                        .rev()
                        .collect::<Vec<_>>())
                })
                .map_err(|err| -> Template {
                    println!("Error inserting row: {:?}", err);
                    Template::render(
                        "errors",
                        &TemplateError {
                            error_type: "Database Error".to_string(),
                            error_detail: format!("Error inserting row: {:?}", err),
                        },
                    )
                })?;
            Ok(Redirect::to("/"))
        }
        Err(FormDataError::Io(_)) => Err(Template::render(
            "errors",
            &TemplateError {
                error_type: "Form Error".to_string(),
                error_detail: "Form input was invalid UTF-8.".to_string(),
            },
        )),
        Err(FormDataError::Malformed(f)) | Err(FormDataError::Parse(_, f)) => {
            Err(Template::render(
                "errors",
                &TemplateError {
                    error_type: "Form Error".to_string(),
                    error_detail: format!("Invalid form input: {:?}", f),
                },
            ))
        }
    }
}

#[derive(Debug, FromForm, Deserialize)]
pub struct CategoryForm {
    name: String,
    description: String,
}

#[post("/add_category", data = "<todo_data>")]
pub fn add_category(
    conn: DbConn,
    todo_data: Result<Form<CategoryForm>, FormError>,
) -> Result<Redirect, Template> {
    match todo_data {
        Ok(form) => {
            let category = NewCategory {
                name: &form.name,
                discription: &form.description,
            };
            diesel::insert_into(schema::categories::table)
                .values(&category)
                .execute(&conn.0)
                .map_err(|err| -> Template {
                    println!("Error inserting row: {:?}", err);
                    Template::render(
                        "errors",
                        &TemplateError {
                            error_type: "Database Error".to_string(),
                            error_detail: format!("Error inserting row: {:?}", err),
                        },
                    )
                })?;
            Ok(Redirect::to("/"))
        }
        Err(FormDataError::Io(_)) => Err(Template::render(
            "errors",
            &TemplateError {
                error_type: "Form Error".to_string(),
                error_detail: "Form input was invalid UTF-8.".to_string(),
            },
        )),
        Err(FormDataError::Malformed(f)) | Err(FormDataError::Parse(_, f)) => {
            Err(Template::render(
                "errors",
                &TemplateError {
                    error_type: "Form Error".to_string(),
                    error_detail: format!("Invalid form input: {:?}", f),
                },
            ))
        }
    }
}

#[derive(Debug, FromForm, Deserialize)]
pub struct DoneForm {
    id: i32,
    action: String,
}

#[post("/complete_todo", data = "<done_data>")]
pub fn done_action(
    conn: DbConn,
    done_data: Result<Form<DoneForm>, FormError>,
) -> Result<Redirect, Template> {
    match done_data {
        Ok(form) => {
            if form.action.as_str() == "completed" {
                diesel::update(tasks::table.filter(tasks::id.eq(form.id)))
                    .set(
                        tasks::done.eq(SystemTime::now()
                            .duration_since(SystemTime::UNIX_EPOCH)
                            .unwrap()
                            .as_secs() as i32),
                    )
                    .execute(&conn.0)
                    .map_err(|err| -> Template {
                        println!("Error Updating row: {:?}", err);
                        Template::render(
                            "errors",
                            &TemplateError {
                                error_type: "Database Error".to_string(),
                                error_detail: format!("Error updating row: {:?}", err),
                            },
                        )
                    })?;
            }
            Ok(Redirect::to("/"))
        }
        Err(FormDataError::Io(_)) => Err(Template::render(
            "errors",
            &TemplateError {
                error_type: "Form Error".to_string(),
                error_detail: "Form input was invalid UTF-8.".to_string(),
            },
        )),
        Err(FormDataError::Malformed(f)) | Err(FormDataError::Parse(_, f)) => {
            Err(Template::render(
                "errors",
                &TemplateError {
                    error_type: "Form Error".to_string(),
                    error_detail: format!("Invalid form input: {:?}", f),
                },
            ))
        }
    }
}
