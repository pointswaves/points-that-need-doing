#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate serde_derive;

extern crate markdown;

use rocket_contrib::serve::StaticFiles;
use rocket_contrib::templates::Template;
use std::env;

pub mod cors;
pub mod models;
pub mod routes_api;
pub mod routes_auth;
pub mod routes_site;
pub mod schema; // Ignore errors from this for now; it doesn't get created unti later

// This registers your database with Rocket, returning a `Fairing` that can be `.attach`'d to your
// Rocket application to set up a connection pool for it and automatically manage it for you.
#[database("sqlite_tasks")]
pub struct DbConn(diesel::SqliteConnection);

fn main() {
    rocket::ignite()
        .mount(
            "/public",
            StaticFiles::from(
                env::var("ROCKET_STATIC_DIR")
                    .ok()
                    .unwrap_or_else(|| "./static".to_string()),
            ),
        )
        .mount(
            "/api_v1",
            routes![routes_api::create_task, routes_api::list_tasks,],
        )
        .mount(
            "/",
            routes![
                routes_site::index,
                routes_site::task_all,
                routes_site::task_by_id,
                routes_site::add_todo,
                routes_site::add_category,
                routes_site::add_todo_form,
                routes_site::add_category_form,
                routes_site::done_action,
                routes_auth::login,
                routes_auth::login_form,
                routes_auth::register,
                routes_auth::register_form,
                routes_auth::logout,
                routes_auth::user_id,
                routes_auth::user_id_gard,
                routes_auth::group_make,
                routes_auth::group_form,
                routes_auth::group_list,
                routes_auth::group_add_user,
            ],
        )
        .attach(DbConn::fairing())
        .attach(cors::CorsFairing)
        .attach(Template::fairing())
        .launch();
}
