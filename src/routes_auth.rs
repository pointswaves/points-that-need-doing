use crate::rocket::outcome::IntoOutcome;
use diesel::{self, prelude::*};
use rocket::http::{Cookie, Cookies};
use rocket::request::{self, FromRequest, Request};
use rocket::request::{Form, FormError};
use rocket::response::{Flash, Redirect};
use rocket_contrib::templates::Template;
use sodiumoxide::crypto::pwhash::argon2id13;
use std::collections::HashMap;
use std::time::SystemTime;

use crate::models::{GroupMember, NewGroupMember, NewTodogroup, NewUser, Todogroup, User};
use crate::schema;
use crate::schema::users;
use crate::DbConn;

const USER_COOKIE_API_VERSION_MAJOR: i32 = 1;
const USER_COOKIE_API_VERSION_MINOR: i32 = 0;

#[derive(Serialize, Deserialize, Debug)]
pub struct LogedInUser {
    user_id: i32,
    api_version_major: i32,
    api_version_minor: i32,
    created: i32,
}

impl LogedInUser {
    pub fn id(&self) -> i32 {
        self.user_id
    }

    pub fn new(id: i32) -> Self {
        LogedInUser {
            user_id: id,
            api_version_major: USER_COOKIE_API_VERSION_MAJOR,
            api_version_minor: USER_COOKIE_API_VERSION_MINOR,
            created: SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs() as i32,
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for LogedInUser {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, ()> {
        let parsed_self: Self = request
            .cookies()
            .get_private("user_id")
            .and_then(|cookie| serde_json::from_str(cookie.value()).ok())
            .or_forward(())?;
        if parsed_self.api_version_major != USER_COOKIE_API_VERSION_MAJOR {
            return rocket::Outcome::Forward(());
        };
        rocket::Outcome::Success(parsed_self)
    }
}

#[derive(Debug, Serialize)]
struct TemplateError {
    error_type: String,
    error_detail: String,
}
/// Retrieve the user's ID, if any.
#[get("/user_id")]
pub fn user_id(mut cookies: Cookies) -> Option<String> {
    cookies
        .get_private("user_id")
        .map(|cookie| format!("User ID: {}", cookie.value()))
}

#[get("/user_id_alt")]
pub fn user_id_gard(user: LogedInUser) -> Option<String> {
    Some(format!("User ID: {:?}", user))
}

#[get("/register")]
pub fn register_form() -> Template {
    let context: HashMap<&str, &str> = HashMap::new();
    Template::render("newuser_form", &context)
}

#[derive(Debug, FromForm, Deserialize)]
pub struct NewUserForm {
    name: String,
    password: String,
    email: String,
}

#[post("/register", data = "<newuser>")]
pub fn register(
    newuser: Result<Form<NewUserForm>, FormError>,
    conn: DbConn,
) -> Result<Redirect, Template> {
    if let Ok(valid_newuser) = newuser {
        sodiumoxide::init().unwrap();
        let hash = argon2id13::pwhash(
            valid_newuser.password.as_bytes(),
            argon2id13::OPSLIMIT_INTERACTIVE,
            argon2id13::MEMLIMIT_INTERACTIVE,
        )
        .map_err(|err| -> Template {
            println!("Error hashing password: {:?}", err);
            Template::render(
                "errors",
                &TemplateError {
                    error_type: "Error hashing password".to_string(),
                    error_detail: format!("Error: {:?}", err),
                },
            )
        })?;
        let texthash = std::str::from_utf8(&hash.0).unwrap().to_string();
        let user = NewUser {
            name: &valid_newuser.name,
            password: &texthash,
            email: &valid_newuser.email,
            validated: true,
            active: true,
            admin: false,
        };
        diesel::insert_into(schema::users::table)
            .values(&user)
            .execute(&conn.0)
            .map_err(|err| -> Template {
                println!("Error inserting row: {:?}", err);
                Template::render(
                    "errors",
                    &TemplateError {
                        error_type: "Database Error".to_string(),
                        error_detail: format!("Error inserting row: {:?}", err),
                    },
                )
            })?;
        Ok(Redirect::to("/login"))
    } else {
        Err(Template::render(
            "errors",
            &TemplateError {
                error_type: "Form Error".to_string(),
                error_detail: "Oh dere".to_string(),
            },
        ))
    }
}

#[derive(Debug, Serialize)]
struct TemplateUser {
    name: String,
    id: i32,
}

#[derive(Debug, Serialize)]
struct TemplateGroup<'a> {
    name: String,
    des: String,
    id: i32,
    user_list: Vec<TemplateUser>,
    add_user: Option<&'a Vec<TemplateUser>>,
}

#[derive(Debug, Serialize)]
struct TemplateGroups<'a> {
    group_list: Vec<TemplateGroup<'a>>,
}

#[get("/groups")]
pub fn group_list(user: LogedInUser, conn: DbConn) -> Template {
    let user_list: Vec<User> = schema::users::dsl::users.load(&conn.0).unwrap();
    let user_templates: Vec<TemplateUser> = user_list
        .iter()
        .filter_map(|other_user| {
            if other_user.id != user.id() {
                Some(TemplateUser {
                    name: other_user.name.clone(),
                    id: other_user.id,
                })
            } else {
                None
            }
        })
        .collect();
    let join_members = schema::group_members::table.inner_join(schema::todogroups::table);
    let join_groups = schema::group_members::table
        .inner_join(schema::todogroups::table)
        .inner_join(schema::users::table);
    let todo_groups: std::vec::Vec<(GroupMember, Todogroup)> = join_members
        .filter(schema::group_members::user.eq(user.user_id))
        .load(&conn.0)
        .unwrap();
    let group_list = todo_groups
        .into_iter()
        .map(|(group_member, todogroup)| {
            let member_list: std::vec::Vec<(GroupMember, Todogroup, User)> = join_groups
                .filter(schema::todogroups::id.eq(todogroup.id))
                .load(&conn.0)
                .unwrap();
            let user_list = member_list
                .into_iter()
                .map(|(_, _, user)| TemplateUser {
                    name: user.name,
                    id: user.id,
                })
                .collect::<Vec<TemplateUser>>();
            let user_template_option = if group_member.admin {
                Some(&user_templates)
            } else {
                None
            };
            TemplateGroup {
                name: todogroup.name,
                des: todogroup.discription,
                id: todogroup.id,
                user_list,
                add_user: user_template_option,
            }
        })
        .collect::<Vec<TemplateGroup>>();
    let context = TemplateGroups { group_list };
    Template::render("list_group", &context)
}

#[derive(Debug, FromForm, Deserialize)]
pub struct AddGroupUser {
    group_id: i32,
    user_id: i32,
}

#[post("/add_user_to_group", data = "<add_user_data>")]
pub fn group_add_user(
    add_user_data: Result<Form<AddGroupUser>, FormError>,
    conn: DbConn,
    _user: LogedInUser,
) -> Result<Redirect, Template> {
    if let Ok(user_data) = add_user_data {
        let new_group_member = NewGroupMember {
            user: user_data.user_id,
            todogroup: user_data.group_id,
            admin: true,
        };
        diesel::insert_into(schema::group_members::dsl::group_members)
            .values(&new_group_member)
            .execute(&conn.0)
            .unwrap();
        Ok(Redirect::to("/groups"))
    } else {
        Err(Template::render(
            "errors",
            &TemplateError {
                error_type: "Form Error".to_string(),
                error_detail: "Oh dere".to_string(),
            },
        ))
    }
}

#[derive(Debug, FromForm, Deserialize)]
pub struct NewGroupForm {
    name: String,
    discription: String,
}

#[get("/add_groups")]
pub fn group_form(_user: LogedInUser) -> Template {
    let context: HashMap<&str, &str> = HashMap::new();
    Template::render("group_form", &context)
}

#[post("/add_groups", data = "<newgroup_data>")]
pub fn group_make(
    newgroup_data: Result<Form<NewGroupForm>, FormError>,
    conn: DbConn,
    user: LogedInUser,
) -> Result<Redirect, Template> {
    if let Ok(valid_group) = newgroup_data {
        let new_group = NewTodogroup {
            name: &valid_group.name,
            discription: &valid_group.discription,
            public_visable: false,
        };
        use crate::diesel::Connection;
        use diesel::result::Error;
        let inserted_groups: std::vec::Vec<Todogroup> = conn
            .0
            .transaction::<_, Error, _>(|| {
                let inserted_count = diesel::insert_into(schema::todogroups::dsl::todogroups)
                    .values(&new_group)
                    .execute(&conn.0)?;

                Ok(schema::todogroups::dsl::todogroups
                    .order(schema::todogroups::dsl::id.desc())
                    .limit(inserted_count as i64)
                    .load(&conn.0)?
                    .into_iter()
                    .rev()
                    .collect::<Vec<_>>())
            })
            .map_err(|err| -> Template {
                println!("Error inserting row: {:?}", err);
                Template::render(
                    "errors",
                    &TemplateError {
                        error_type: "Database Error".to_string(),
                        error_detail: format!("Error inserting row: {:?}", err),
                    },
                )
            })?;
        let new_group_member = NewGroupMember {
            user: user.user_id,
            todogroup: inserted_groups.first().unwrap().id,
            admin: true,
        };
        diesel::insert_into(schema::group_members::dsl::group_members)
            .values(&new_group_member)
            .execute(&conn.0)
            .unwrap();
        Ok(Redirect::to("/"))
    } else {
        Err(Template::render(
            "errors",
            &TemplateError {
                error_type: "Form Error".to_string(),
                error_detail: "Oh dere".to_string(),
            },
        ))
    }
}

#[get("/login")]
pub fn login_form() -> Template {
    let context: HashMap<&str, &str> = HashMap::new();
    Template::render("login_form", &context)
}

#[derive(Debug, FromForm, Deserialize)]
pub struct LoginForm {
    email: String,
    password: String,
}

pub fn verify(hash: &str, passwd: &str) -> bool {
    sodiumoxide::init().unwrap();
    let mut padded = [0u8; 128];
    hash.as_bytes().iter().enumerate().for_each(|(i, val)| {
        padded[i] = *val;
    });
    match argon2id13::HashedPassword::from_slice(&padded) {
        Some(hp) => argon2id13::pwhash_verify(&hp, passwd.as_bytes()),
        _ => false,
    }
}

#[post("/login", data = "<login_data>")]
pub fn login(
    login_data: Result<Form<LoginForm>, FormError>,
    mut cookies: Cookies,
    conn: DbConn,
) -> Result<Flash<Redirect>, Template> {
    if let Ok(login) = login_data {
        println!("Name: {:?}", login.email);
        let user_vec = users::table
            .filter(users::email.eq(login.email.to_string()))
            .load::<User>(&conn.0)
            .map_err(|err| -> Template {
                println!("Error getting todo items from the database: {:?}", err);
                Template::render(
                    "errors",
                    &TemplateError {
                        error_detail: format!("Error getting user from the database: {:?}", err),
                        error_type: "Database Error:".into(),
                    },
                )
            })?;
        if let Some(user) = user_vec.first() {
            if verify(&user.password, &login.password) {
                let user_cookie = LogedInUser::new(user.id);
                let user_cookie_string: String =
                    serde_json::to_string(&user_cookie).map_err(|_| {
                        Template::render(
                            "errors",
                            TemplateError {
                                error_type: "Could not serialise cookie".to_string(),
                                error_detail: "serde error".to_string(),
                            },
                        )
                    })?;
                cookies.add_private(Cookie::new("user_id", user_cookie_string));
                Ok(Flash::success(Redirect::to("/"), "Successfully logged in"))
            } else {
                Err(Template::render(
                    "errors",
                    TemplateError {
                        error_type: "Bad Password".to_string(),
                        error_detail: "Did you forget".to_string(),
                    },
                ))
            }
        } else {
            Err(Template::render(
                "errors",
                TemplateError {
                    error_type: "User not found".to_string(),
                    error_detail: "User not found".to_string(),
                },
            ))
        }
    } else {
        //Err(Box::new(Flash::error(Redirect::to("/"), "Invalid username.")))
        Err(Template::render(
            "errors",
            TemplateError {
                error_type: "Bad From".to_string(),
                error_detail: "badform".to_string(),
            },
        ))
    }
}

/// Remove the `user_id` cookie.
#[get("/logout")]
pub fn logout(mut cookies: Cookies) -> Flash<Redirect> {
    cookies.remove_private(Cookie::named("user_id"));
    Flash::success(Redirect::to("/"), "Successfully logged out.")
}
