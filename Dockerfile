FROM rustlang/rust:nightly

RUN cargo install diesel_cli
RUN apt-get update \
    && apt-get install -y ca-certificates tzdata sqlite3 libmariadb3 \
    && rm -rf /var/lib/apt/lists/*

RUN USER=root cargo new --bin rocket_diesel
WORKDIR ./rocket_diesel
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock
COPY ./rust-toolchain ./rust-toolchain
RUN cargo build --release
RUN rm src/*.rs

COPY ./src/* ./src/
COPY ./diesel.toml ./diesel.toml
COPY ./Rocket.toml ./Rocket.toml

RUN rm ./target/release/deps/rocket_diesel*
RUN cargo build --release

#FROM debian:buster-slim
ARG APP=/usr/src/app

EXPOSE 8000

ENV TZ=Etc/UTC \
    APP_USER=appuser \
    ROCKET_TEMPLATE_DIR="/var/pointstodo/templates" \
    ROCKET_STATIC_DIR="/var/pointstodo/static"


RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${APP} \
    && mkdir -p /var/pointstodo/templates \
    && mkdir -p /var/pointstodo/static

RUN cp /rocket_diesel/target/release/rocket_diesel ${APP}/rocket_diesel
COPY ./templates /var/pointstodo/templates
COPY ./static /var/pointstodo/static
COPY ./migrations /var/pointstodo/migrations

RUN chown -R $APP_USER:$APP_USER ${APP}

USER $APP_USER
WORKDIR ${APP}

CMD ["./rocket_diesel"]
