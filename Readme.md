Basic use age
=============

Run the server
--------------

First run the server

    cargo run

Use the api
-----------

Add some data

    curl -X POST -H "Content-Type: application/json" -d '{"name": "Do something", "discription": "Its important to do", "category": 0, "created": 0}' http://localhost:8000/api_v1/create_task

You can also add data with the category set to null my missing it out from the json

    curl -X POST -H "Content-Type: application/json" -d '{"name": "Do something", "discription": "Its important to do", "created": 0}' http://localhost:8000/api_v1/create_task

read it back

    curl http://localhost:8000/api_v1/list_tasks

Use the website
---------------

Browser freindly pages can be found at the servers route

    firefox http://localhost:8000

They have basic css pages etc to not look too plane, with a base template to apply them consistenly.

Docker setup
============

Interactive

    podman run -e ROCKET_ENV=development  -v dockertmp:/var/ptd/ -it registry.gitlab.com/pointswaves/points-that-need-doing:latest bash

setup

    podman run -e ROCKET_ENV=development -e DATABASE_URL=/var/ptd/database.sqlite  -v dockertmp:/var/ptd/  localhost/localtodo:testing  diesel migration run --migration-dir /rocket_diesel/migrations

run
    podman run -e ROCKET_ENV=development -e ROCKET_ADDRESS="0.0.0.0" -e ROCKET_DATABASES='{sqlite_tasks={url="/var/ptd/database.sqlite"}}' -e ROCKET_TEMPLATE_DIR="/var/pointstodo/templates" --name local_ptd -p 8000:8000  -v dockertmp:/var/ptd/  localhost/localtodo:testing  ./rocket_diesel


    podman run -e ROCKET_ENV=development -e ROCKET_ADDRESS="0.0.0.0" -e ROCKET_DATABASES='{sqlite_tasks={url="/var/ptd/database.sqlite"}}' -e ROCKET_TEMPLATE_DIR="/var/pointstodo/templates" -e ROCKET_STATIC_DIR="/var/pointstodo/static" --name local_ptd -p 8000:8000  -v dockertmp:/var/ptd/  localhost/localtodo:testing  ./rocket_diesel

    podman run -e ROCKET_ENV=development -e ROCKET_ADDRESS="0.0.0.0" -e ROCKET_DATABASES='{sqlite_tasks={url="/var/ptd/database.sqlite"}}' --name local_ptd -p 8000:8000  -v dockertmp:/var/ptd/  localhost/localtodo:testing  ./rocket_diesel


cleanup
    podman stop local_ptd

    podman rm $(podman ps -a -q)

build
    podman build . --tag localtodo:testing



