-- Your SQL goes here
CREATE TABLE todogroups (
    id INTEGER  NOT NULL PRIMARY KEY,
    name VARCHAR NOT NULL,
    discription TEXT NOT NULL,
    public_visable BOOLEAN NOT NULL
);


CREATE TABLE group_members (
    user  INTEGER  NOT NULL,
    todogroup  INTEGER  NOT NULL,
    admin BOOLEAN NOT NULL,
    PRIMARY KEY  (user, todogroup),
    FOREIGN KEY  (user)  REFERENCES  users (id),
    FOREIGN KEY  (todogroup)  REFERENCES  todogroups (id)
);