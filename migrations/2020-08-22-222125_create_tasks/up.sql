CREATE TABLE tasks (
	id INTEGER NOT NULL PRIMARY KEY,
	name VARCHAR NOT NULL,
	discription TEXT NOT NULL,
	discription_cache TEXT NOT NULL,
	created INT NOT NULL,
	started INT,
	due INT,
	category INT,
	parent INT,
	done INT,
	todogroup INT NOT NULL,
	FOREIGN KEY  (category)  REFERENCES categories (id)
);
